import * as React from 'react';
import { Image, AppRegistry, StyleSheet, View } from 'react-native';
import { Tabs } from 'src/Tabs';
import icons, { tint } from 'src/icons';
import Text from 'src/reasonable/text';
import SwipeMenu from 'src/SwipeMenu';

const styles = StyleSheet.create({
  home: {
    paddingTop: 44,
    flex: 1,
  },
});

const Home = () => {
  const tab1 = {
    key: 'Home',
    node: <Text>This is where the home calendar will go.</Text>,
    icon: <Image {...tint(icons.home, 'white')} />,
    title: 'Home',
  };
  const tab2 = {
    key: 'Work',
    node: <Text>This is where the work calendar will go.</Text>,
    icon: <Image {...tint(icons.work, 'white')} />,
    title: 'Work',
  };
  const tab3 = {
    key: 'Secretsss',
    node: <Text>shhhhh some secrets</Text>,
    icon: <Image {...tint(icons.work, 'white')} />,
    title: 'Secretsss',
  };

  const menu = new SwipeMenu()

  return (
    <View style={styles.home} {...menu.handlers()}>
      {menu}
      <Tabs tabs={[tab1, tab2, tab3]} />
    </View>
  );
};

AppRegistry.registerComponent('spekkio', () => Home);
