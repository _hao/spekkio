import * as React from 'react';
import { StyleSheet, View, Animated } from 'react-native';
import * as colors from 'src/colors';
import Text from 'src/reasonable/text';
import Button from 'src/reasonable/button';

export interface Tab {
  node: any;
  icon: any;
  key: string;
  title: string;
}

export interface TabsProps {
  tabs: Tab[];
}

export interface PureTabsProps extends TabsProps {
  selectedTabIndex: number;
  onSelectTab: (index: number) => void;
  backgroundColors: Animated.AnimatedInterpolation[];
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  active: {
    backgroundColor: colors.crimson,
  },
  button: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonUsually: {
    borderLeftWidth: 1,
    borderLeftColor: 'white',
  },
  content: {
    flex: 2,
  },
  buttons: {
    flexDirection: 'row',
    height: 64,
  },
  buttonText: {
    color: 'white',
    textAlign: 'center',
    marginTop: 5,
  },
});

export const PureTabs: React.SFC<PureTabsProps> = ({ selectedTabIndex, tabs, onSelectTab, backgroundColors }) => {
  const buttons = tabs.map((tab, index) => (
    <Button
      style={[styles.button, { backgroundColor: backgroundColors[index] }, index === 0 ? null : styles.buttonUsually]}
      key={tab.key}
      onTap={() => onSelectTab(index)}
    >
      {tab.icon}
      <Text style={[styles.buttonText]}>{tab.title}</Text>
    </Button>
  ));
  return (
    <View style={styles.root}>
      <View style={styles.content}>{tabs[selectedTabIndex].node}</View>
      <View style={styles.buttons}>{buttons}</View>
    </View>
  );
};

interface State {
  selectedTabIndex: number;
}

export class Tabs extends React.Component<TabsProps, State> {
  backgroundColorFloats: Animated.Value[];
  backgroundColors: Animated.AnimatedInterpolation[];

  constructor(props: TabsProps) {
    super(props);
    this.state = { selectedTabIndex: 0 };
    this.onSelectTab = this.onSelectTab.bind(this);

    this.backgroundColorFloats = this.props.tabs.map((_, i) => new Animated.Value(i === 0 ? 1 : 0));
    this.backgroundColors = this.backgroundColorFloats.map(value =>
      value.interpolate({
        inputRange: [0, 1],
        outputRange: [colors.bloodRed, colors.crimson],
      })
    );
  }

  render() {
    return (
      <PureTabs
        backgroundColors={this.backgroundColors}
        onSelectTab={this.onSelectTab}
        {...this.state}
        {...this.props}
      />
    );
  }

  onSelectTab(index: number) {
    this.setState({ ...this.state, selectedTabIndex: index });

    for (const [i, float] of this.backgroundColorFloats.entries()) {
      Animated.timing(float, {
        toValue: i === index ? 1 : 0,
        duration: 100,
      }).start();
    }
  }
}
