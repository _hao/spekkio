import * as React from 'react';
import { Text, TextStyle } from 'react-native';

export interface ReasonableTextProps {
  style?: TextStyle[];
  children: string;
}

export const apostrophize = (input: string): string => input.replace(/(\w)'s\b/, '$1’s');

const ReasonableText: React.SFC<ReasonableTextProps> = ({ children, style, ...rest }) => (
  <Text style={[...(style ? style : []), { fontFamily: 'System' }]} {...rest}>
    {apostrophize(children)}
  </Text>
);

export default ReasonableText;
