import * as React from 'react';
import { TouchableWithoutFeedback, Animated } from 'react-native';

export interface ButtonProps {
  children: any;
  onTap: () => void;
  style: any;
}

class Button extends React.Component<ButtonProps> {
  top: Animated.Value;

  constructor(props: ButtonProps) {
    super(props);
    this.top = new Animated.Value(0);
    this.onPressIn = this.onPressIn.bind(this);
    this.onPressOut = this.onPressOut.bind(this);
  }

  render() {
    const animation = {
      top: this.top,
      marginBottom: -5,
      paddingBottom: 5,
    };

    return (
      <TouchableWithoutFeedback onPress={this.props.onTap} onPressIn={this.onPressIn} onPressOut={this.onPressOut}>
        <Animated.View style={[this.props.style, animation]}>{this.props.children}</Animated.View>
      </TouchableWithoutFeedback>
    );
  }

  onPressIn() {
    this.top.setValue(5);
  }

  onPressOut() {
    Animated.spring(this.top, {
      toValue: 0,
      bounciness: 8,
      speed: 12,
    }).start();
  }
}

export default Button;
