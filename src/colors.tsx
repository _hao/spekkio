export const bloodRed = '#3A0713';

export const crimson = '#6D0C0C';

export const pink = '#FFCCD3';

export const brown = '#C19166';

export const yellow = '#FFF2BA';
