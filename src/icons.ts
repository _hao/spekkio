const size = (width: number, height: number) => ({ width, height });
const uri = (name: string) => ({ uri: name });

export const tint = (icon: any, tintColor: string) => ({
  ...icon,
  style: { ...icon.style, tintColor },
});

const icons = {
  home: {
    source: uri('icon-home'),
    style: size(24, 24),
  },
  work: {
    source: uri('icon-work'),
    style: size(24, 24),
  },
};

export default icons;
